import "./index.styl"

import {KinematicRender} from "./render/kinematic.js"
import {BaseRender} from "./render/render.js"

'use strict';

let area = document.querySelector("#cinematic-area");
const kinematic = new KinematicRender(area, "../finalDarwin.babylon");
kinematic.init();

// X-Z-Y, cистема координат blender (против часовой)

//Вертим голову
kinematic.doAction('head', 1.0, -Math.PI/6, 0, 0);
kinematic.doAction('neck', 1.0, 0, Math.PI/6, 0);

//Вертим ногу
kinematic.doAction('leg.l.hip', 1.0, 0, -Math.PI/8, 0);
kinematic.doAction('leg.l.haunch', 1.0, Math.PI/4, 0, 0);
kinematic.doAction('leg.l.shin', 1.0, -Math.PI/4, 0, 0);
kinematic.doAction('leg.l.foot', 1.0, 0, 0, Math.PI/6);

//Вертим руку
kinematic.doAction('hand.l.forearm', 1.0, 0, 0, Math.PI/6);
kinematic.doAction('hand.l.cubit', 1.0, Math.PI/4, 0, 0);
kinematic.doAction('hand.l.spoke', 1.0, 0, Math.PI/4, 0);
