/* Created by user on 13.07.2016.
 * Палитра оттенков цветов в формате RGB
*/

'use strict';

export const RED = { r: 1.0, g: 0.0, b: 0.0};
export const GREEN = { r: 0.0, g: 1.0, b: 0.0};
export const BLUE = { r: 0.0, g: 0.0, b: 1.0};
export const DARKSLATEBLUE = { r: 72 / 255, g: 70 / 255, b: 107 / 255};