/* Created by user on 13.07.2016. 
 * Функция separate для удобного разбиения строк, в данном случае - для получения директории и имени файла .babylon
*/

'use strict';

export function separate(path) {
    return new class {
        toFilePath() {
            if (path.toString() === undefined) {
                console.log("WARNING: Неподходящий тип для пути файла");
            }
            if (path.toString() === "") {
                console.log("WARNING: Задан пустой путь");
            }

            let separatedPath = path.split('/');
            let fileFolder = "";
            for(let i = 0; i < separatedPath.length - 1; ++i) {
                fileFolder += separatedPath[i];
            }
            fileFolder += '/';
            let fileName = separatedPath[separatedPath.length - 1];

            return {
                fileFolder: fileFolder,
                fileName: fileName
            }
        }
    }
}