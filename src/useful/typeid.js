/* Created by user on 13.07.2016. 
 * Функция для выведения типа данных, возвращает в виде строки
*/

'use strict';

export function getTypeName(object) {
    if (typeof object !== 'object') {
        return typeof object;
    } else {
        return object.toString();
    }
}