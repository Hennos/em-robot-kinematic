/* Created by user on 03.07.2016.
 * Модуль содержит класс, выполняющий рендеринг базовой сцены в указанной области, используя babylon.js.
 */

import BABYLON from 'babylonjs'

import {getTypeName} from "../useful/typeid.js"

'use strict';

export class BaseRender {
    constructor(_area) {
        if (!BABYLON.Engine.isSupported()) {
            throw new Error("Babylon.js не поддерживается браузером");
        }
        if (arguments.length == 0) {
            console.log("Конструктор рендера вызван без параметров");
        }

        this.area = _area;
        this._engine = new BABYLON.Engine(_area, true);
        this._engine.enableOfflineSupport = false;
        this._scene = null;
    }

    get area() {
        if (!(this._area instanceof HTMLCanvasElement)) {
            console.log("WARNING: Область типа " + getTypeName(this._area) + " может привести к ошибке");
        }

        return this._area;
    }
    set area(_area) {
        if (!(_area instanceof HTMLCanvasElement)) {
            console.log("WARNING: Область типа " + getTypeName(_area) + " не поддерживается");
        }

        if (this._area !== _area) {
            this._area = _area;
        }
    }
    get engine() {
        return this._engine;
    }
    get scene() {
        if (!this._scene) {
            console.log("WARNING: Сцена ещё не загружена");
        }

        return this._scene;
    }

    init() {
        if (!(this._area instanceof HTMLCanvasElement)) {
            throw new Error(
                "Область типа " + getTypeName(this._area) + " не может быть использована для отрисовки");
        }

        this._createScene().then(
            scene => {
                this._scene = scene;
                // Генерирует событие после успешной загрузки сцены
                let loadSceneEvent = new Event('onloadScene');
                this._area.dispatchEvent(loadSceneEvent);
                this.engine.runRenderLoop(this._renderScene(scene));
                window.addEventListener('resize', () => this.engine.resize());
            }
        );
    }

    _updateScene(updater) {
        this._area.addEventListener('onloadScene', updater.bind(this));
    }

    _createScene() {
        return new Promise((resolve) => {
            let scene = new BABYLON.Scene(this.engine, true);
            scene.crearColor = new BABYLON.Color3(0, 1, 0);
            let camera = new BABYLON.FreeCamera('camera1', new BABYLON.Vector3(0, 5, -10), scene);
            camera.setTarget(BABYLON.Vector3.Zero());
            let light = new BABYLON.HemisphericLight('light1', new BABYLON.Vector3(0, 1, 0), scene);
            light.intensity = .5;
            let sphere = new BABYLON.Mesh.CreateSphere('sphere1', 16, 2, scene);
            sphere.position.y = 1;
            let ground = new BABYLON.Mesh.CreateGround('ground1', 6, 6, 2, scene);
            resolve(scene);
        });
    }

    _renderScene(scene) {
        return () => scene.render();
    }
}
