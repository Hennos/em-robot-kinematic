/* Created by user on 04.07.2016.
 * Модуль содержит класс, унаследованный от Render и
 * выполняющий рендеринг сцены кинематики в указанной области, используя babylon.js.
 */

import BABYLON from 'babylonjs'

import {BaseRender} from "./render.js"
import {separate} from "../useful/separate.js"
import {showWorldAxis} from "../useful/axis.js"

import {DARKSLATEBLUE} from "../useful/colorPallete.js"

'use strict';

const DEFAULT_FPS = 24;
const MINIMAL_FPS = 24;
const DEFAULT_ANIMATION_SPEED = 1;
const MINIMAL_ANIMATION_SPEED = 0.1;
const MAXIMUM_ANIMATION_SPEED = 5;

export class KinematicRender extends BaseRender {
    constructor(_area, _path, _fps = DEFAULT_FPS) {
        if (!arguments.length) {
            throw new Error("Конструктор рендера кинематики вызван без параметров");
        }
        if (_path.toString() === undefined) {
            throw new Error("Неккоректный путь до файла");
        }
        if (_path.toString() === '') {
            throw new Error("Неккоректный путь до файла");
        }
        if (_fps < MINIMAL_FPS) {
            console.log("WARNING: Заданная частота кадров меньше допустимой." +
                `Установлено ближайшее допустимое значение: ${MINIMAL_FPS}`);
            _fps = DEFAULT_FPS;
        }

        super(_area);
        this._fileFolder = separate(_path).toFilePath().fileFolder;
        this._fileName = separate(_path).toFilePath().fileName;
        this._fps = _fps;
        this._frames = 0;
    }

    get fileFolder() {
        return this._fileFolder;
    }
    get fileName() {
        return this._fileName;
    }
    get fps() {
        return this._fps;
    }
    get frame() {
        return this._frames;
    }

    doAction(bName, aSpeed = DEFAULT_ANIMATION_SPEED, aX = 0, aY = 0, aZ = 0) {
        if (!arguments.length) {
            console.log("WARNING: Не заданны параметры действия");
            return false;
        }
        if (typeof bName !== 'string') {
            console.log("WARNING: Имя поворачиваемой кости должно быть строкой");
            return false;
        }
        if (aSpeed < MINIMAL_ANIMATION_SPEED) {
            console.log("WARNING: Заданная скорость проигрывания анимации меньше допустимой." +
                `Установлено ближайшее допустимое значение: ${MINIMAL_ANIMATION_SPEED}`);
        }
        if (aSpeed > MAXIMUM_ANIMATION_SPEED) {
            console.log("WARNING: Заданная скорость проигрывания анимации больше допустимой." +
                `Установлено ближайшее допустимое значение: ${MAXIMUM_ANIMATION_SPEED}`);
        }

        this._updateScene(() => {
            if (!this.scene.getBoneByName(bName)) {
                console.log("WARNING: Кости с таким именем не существует в сцене");
                return false;
            }

            let bone = this.scene.getBoneByName(bName);
            let transformation = (this._rotationJoint(aX, aY, aZ)).multiply(bone.getLocalMatrix());
            let animation = new BABYLON.Animation(
                `a${bName}`,
                '_matrix', this.fps,
                BABYLON.Animation.ANIMATIONTYPE_MATRIX);
            let fFirst = this.frame;
            let fLast = this.frame + this.fps;
            let keys = [];
            keys.push({
                frame: fFirst,
                value: bone.getLocalMatrix()
            });
            keys.push({
                frame: (fFirst - fLast) / 2,
                value: transformation
            });
            keys.push({
                frame: fLast,
                value: bone.getLocalMatrix()
            });
            animation.setKeys(keys);
            bone.animations.push(animation);
            this.scene.beginAnimation(bone, fFirst, fLast, false, aSpeed);
            this._upFrames();
        })
    }

    _createScene() {
        return new Promise((resolve) => {
            let dlCount = 0;
            BABYLON.SceneLoader.Load(
                this.fileFolder, this.fileName, this.engine, (newScene) => {
                    let scene = newScene;
                    scene.executeWhenReady(() => {
                        scene.meshes.forEach((elem) => elem.convertToFlatShadedMesh());
                        scene.clearColor = new BABYLON.Color3(DARKSLATEBLUE.r, DARKSLATEBLUE.g, DARKSLATEBLUE.b);
                        let camera = new BABYLON.FreeCamera('fCam', new BABYLON.Vector3(0, 10, 20), scene);
                        camera.attachControl(this._area);
                        camera.setTarget(BABYLON.Vector3.Zero());
                        let light = scene.lights[0];
                        light.intensity = 1.0;
                        resolve(scene); // Передаём сцену дальше по цепочке callback-ов
                    });
                }, (evt) => {
                    dlCount = evt.loaded / (1024 * 1024);
                    this.engine.loadingUIText =
                        "Загрузка сцены..." + Math.floor(dlCount * 100.0) / 100.0 + " MB уже загружено.";
                }
            );
        });
    }

    _renderScene(_scene) {
        return () => {
            _scene.render();
        }
    }

    // Поворот против часовой стрелки принят за положительное направление
    _rotationJoint(aX, aY, aZ) {
        if (!this.scene) {
            console.log("WARNING: Не удалось совершить поворот. Сцена ещё не готова к использованию");
            return false;
        }

        if (!(aX || aY || aZ)) {
            return BABYLON.Matrix.Identity();
        }
        let rX = BABYLON.Matrix.RotationX(aX);
        let rY = BABYLON.Matrix.RotationY(aY);
        let rZ = BABYLON.Matrix.RotationZ(aZ);
        let rotation = (rX.multiply(rY)).multiply(rZ);
        return rotation;
    }

    // Добавление следующего такта анимации
    _upFrames() {
        this._frames += this.fps;
    }
}